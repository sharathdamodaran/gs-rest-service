package com.tw.bootcamp.controller;

import com.tw.bootcamp.model.Book;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

    @GetMapping("/books")
    public Book getBooks(){
        return new Book("xyz Book");
    }
}
