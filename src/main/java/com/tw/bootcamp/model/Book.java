package com.tw.bootcamp.model;

public class Book {
    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    private String bookName;
    public Book(String bookName) {
        this.bookName = bookName;
    }
}
